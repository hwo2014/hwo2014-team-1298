## The Talking Heads

### Run with Web Interface
From Root/Javascript

#### From Unix
`NODE_ENV=web node index.js`

#### From Windows
set NODE_ENV=web
node index.js

### Run without Web Interface (like CI)
From Repository Root
`./run testserver.helloworldopen.com 8091`



## JavaScript bot for HWO 2014

Requires node/npm (tested with 0.10.26).

### Additional dependencies

If you need any additional dependencies, add them to package.json, then build with `npm install`.

The `node_modules` directory is under version control, so you need to commit the changes there.

When the bot is run on the HWO servers, `npm install` will not be called. Hence all deps must be pre-installed into node_modules.
