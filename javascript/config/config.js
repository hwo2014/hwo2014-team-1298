// This configures the application
// If it's missing the process arguments it defaults to the second option

module.exports = {
    serverHost: process.argv[2] || 'senna.helloworldopen.com',
	serverPort: process.argv[3] || 8091,
	botName: process.argv[4] || 'The Talking Heads',
	botKey: process.argv[5] || 'VVmQgmSHvkwMbg',
	port: 8080,
	root: '/',
	web: process.env.NODE_ENV == 'web'
};