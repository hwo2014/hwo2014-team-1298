// This configures express
// express is a simple html server
// it serves up the web based html/css/js

var express = require( 'express' ),
	config = require( './config' ),
    path = require('path'),
	rootPath = path.normalize(__dirname + '/..');

module.exports = function( app ) {
	app.set( 'showStackerror', true );

	app.set( 'views', rootPath + '/core/views' );
	app.set( 'view engine', 'jade' );
	app.use( express.static( rootPath + '/web/' ) );
	app.use( express.static( rootPath + '/bower_components/' ) );
}