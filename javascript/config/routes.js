// This should be the only route that we need
// this just says that whenever the / is requested
// send 'index' which is the jade view in /core/views/

module.exports = function( app ) {

	app.get( '/', function( req, res ){
		res.render('index');
	});

};