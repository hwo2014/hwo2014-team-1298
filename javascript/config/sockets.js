// This sets up and configures the Socket.io data
// This shouldn't need to change, it's all set up to use
// the normal bot handler.

var http = require( 'http' ),
    sio = require( 'socket.io' ),
    config = require('./config'),
    bot = require('../core/bot/bot' ),
    handler = require('../core/bot/handler'),
    garrett = require('../core/bot/handlers/garrett'),
    jeff = require('../core/bot/handlers/jeff'),
    karl = require('../core/bot/handlers/karl');

module.exports = function( app ) {
	var server = http.createServer( app );	
	var io = sio.listen( server );
	global.io = io;
	server.listen( config.port, function() {
	    console.log( 'Listening (' + app.get( 'env' ) + ') on port ' + config.port );
	});

	io.configure( function() {
		io.set( 'log level', 1 );
		io.set("transports", ["xhr-polling"]);
		io.set("polling duration", 10);
	});

	io.sockets.on( 'connection', function( socket ) {

		var myBot = null;

		socket.on( 'connect', function( botHandler, callback ) {
			console.log('connecting');
			if(!botHandler) {
				myBot = new bot( new handler( socket ) );
			} else {
				switch(botHandler) {
					case 'garrett':
					myBot = new bot( new garrett( socket ) );
					break;
					case 'jeff':
					myBot = new bot( new jeff( socket ) );
					break;
					case 'karl':
					myBot = new bot( new karl( socket ) );
					break;
					default:
					myBot = new bot( new handler( socket ) );
					break;
				}
			}
			myBot.connect();
		});
		socket.on( 'start', function( callback ) {
			if ( !myBot ) callback && callback( 'Not connected' );

		});
	});
};