// This is the bot wrapper
// Nothing in here should need to be modified anymore

var net = require("net"),
  JSONStream = require('JSONStream'),
  config = require('../../config/config' ),
  handler = require( './handler' );

function Bot(messageHandler) {
  this.handler = messageHandler || new handler();
}

Bot.prototype = {
  handler: null,
  client: null,
  jsonStream: null,

  connect: function() {
    var self = this;
    this.handler.setBot( this );

    // Track names:
      // keimola
      // germany
      // usa
      // france

    //this.client = net.connect(config.serverPort, config.serverHost, function() {
    //  return self.send({
    //    msgType: "createRace",
    //    data: {
    //        botId: {
    //            name: config.botName,
    //            key: config.botKey
    //        },
    //        trackName: 'france',
    //        password: 'keimola',
    //        carCount: 1
    //    }
    //  });
    //});
    this.client = net.connect(config.serverPort, config.serverHost, function () {
        return self.send({
            msgType: "join",
            data: {
                name: config.botName,
                key: config.botKey
            }
        });
    });
    this.jsonStream = this.client.pipe(JSONStream.parse());

    this.jsonStream.on('data', function(data) {
      self.onData(data);
    });

    this.jsonStream.on('error', function() {
      self.onError();
    });

  },

  send: function(json) {
    this.client.write(JSON.stringify(json));
    return this.client.write('\n');
  },

  onData: function(data) {
    if( this.handler ) {
      this.handler.process( data );
    }
  },

  onError: function() {
    return console.log("disconnected");
  }
};

module.exports = Bot;
