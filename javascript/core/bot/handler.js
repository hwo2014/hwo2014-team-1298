// This is where all of the magic will happen
// Everything in the process function is our point of process
var Piece = require( './trackPiece' ),
    fs = require( 'fs' );

var Handler = function ( socket ) {
    this.socket = socket;
};

Handler.prototype = {
    bot: null,
    socket: null,
    race: null,
    raceStarted: false,

    deceleration: [0, 0, 0, 21, 16, 13, 11, 11, 8, 6, 5, 4, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],

    me: {
        color: null,
        name: null,
        stats: {
            throttle: 0.0,
            speed: 0,
            gameTick: 0,
            crashes: 0,
            throttleLimit: 12,
            turboLimit: 300,
            distance: 0,
            totalDistance: 0,
            distanceDelta: 0,
            lastCarAngle: 0,
            isSwitching: false,
            isDecel: false,
            prev: {
                gameTick: 0,
                inPieceDistance: 0,
                length: 0,
                lane: 0,
                anlge: 0
            }
        },
        race: {
            prevPiece: {
                index: 0,
                distance: 0
            },
            currentPiece: {
                index: 0,
                distance: 0
            },
            turbo: {
                isAvailable: false,
                isEngaged: false,
                durationMillis: 0,
                durationTicks: 0,
                factor: 0,
                gameTick: 0
            }
        }
    },
    others: [],

    setBot: function ( bot ) {
        this.bot = bot;
    },

    setThrottle: function ( throttle ) {
        this.me.stats.throttle = throttle;
        this.bot.send( {
            msgType: "throttle",
            data: this.me.stats.throttle
        });
        this.socket && this.socket.emit( 'throttle', throttle );
    },

    useTurbo: function () {
        this.me.race.turbo.isAvailable = false;
        this.bot.send( {
            msgType: "turbo",
            data: '(insert tires squealing noise here)'
        });
        this.socket && this.socket.emit( 'turbo' );
    },

    sendPing: function () {
        this.bot.send( {
            msgType: "ping"
        });
        this.socket && this.socket.emit( 'ping' );
    },

    switchLane: function ( direction ) {
        this.bot.send( {
            msgType: "switchLane",
            data: direction
        });
        this.socket && this.socket.emit( 'switchLane', direction );
        this.me.stats.isSwitching = true;
    },

    currentPiece: function () {
        var index = this.me.race.currentPiece.index;
        return new Piece( index, this.race.track.pieces[index], this.me );
    },

    nextPiece: function ( offset ) {
        var index = this.me.race.currentPiece.index;
        if ( offset ) {
            index += offset;
        } else {
            index++;
        }
        var diff = index - ( this.race.track.pieces.length - 1 );
        if ( diff > 0 )
            index = diff;
        return new Piece( index, this.race.track.pieces[index], this.me );
    },
    nextTurnPiece: function () {
        var index = this.me.race.currentPiece.index;
        for ( var i = index + 1; i < this.race.track.pieces.length; i++ ) {
            var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
            if ( possiblePiece.isCurved() ) return possiblePiece;
        }
        for ( var i = 0; i < index; i++ ) {
            var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
            if ( possiblePiece.isCurved() ) return possiblePiece;
        }
    },
    nextSwitchPiece: function () {
        var index = this.me.race.currentPiece.index;
        for ( var i = index + 1; i < this.race.track.pieces.length; i++ ) {
            var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
            if ( possiblePiece.switchFlag ) return possiblePiece;
        }
        for ( var i = 0; i < index; i++ ) {
            var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
            if ( possiblePiece.switchFlag ) return possiblePiece;
        }
    },
    nextTurnPieceAfterSwitch: function () {
        var index = this.nextSwitchPiece().index;
        for ( var i = index + 1; i < this.race.track.pieces.length; i++ ) {
            var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
            if ( possiblePiece.isCurved() ) return possiblePiece;
        }
        for ( var i = 0; i < index; i++ ) {
            var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
            if ( possiblePiece.isCurved() ) return possiblePiece;
        }
    },
    distanceToNextTurnPiece: function ( inPieceDistance ) {
        var index = this.me.race.currentPiece.index;
        var currentPiece = this.currentPiece();
        if ( currentPiece.isStraight() ) {
            var distanceToNextTurn = currentPiece.length - inPieceDistance;
            for ( var i = index + 1; i < this.race.track.pieces.length; i++ ) {
                var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
                if ( possiblePiece.isCurved() ) {
                    return distanceToNextTurn;
                } else {
                    distanceToNextTurn += possiblePiece.length;
                }
            }
            for ( var i = 0; i < index; i++ ) {
                var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
                if ( possiblePiece.isCurved() ) {
                    return distanceToNextTurn;
                } else {
                    distanceToNextTurn += possiblePiece.length;
                }
            }
        } else {
            return -1;
        }
    },
    distanceToEnd: function ( inPieceDistance ) {
        var index = this.me.race.currentPiece.index;
        var currentPiece = this.currentPiece();
        if ( currentPiece.isStraight() ) {
            var distanceToEnd = currentPiece.length - inPieceDistance;
            for ( var i = index + 1; i < this.race.track.pieces.length; i++ ) {
                var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
                if ( possiblePiece.isCurved() ) {
                    return distanceToEnd;
                } else {
                    distanceToEnd += possiblePiece.length;
                }
            }
            return distanceToEnd;
        } else {
            return -1;
        }
    },
    ticksToNextTurnPiece: function () {
        var distanceToNextTurnPiece = this.distanceToNextTurnPiece( this.me.stats.inPieceDistance );
        return distanceToNextTurnPiece / this.me.stats.speed;
    },
    ticksToDecelrateTo: function ( targetSpeed ) {
        var returnTicks = 0;
        var rndTargetSpeed = Math.round( targetSpeed );
        var rndSpeed = Math.round( this.me.stats.speed );
        if ( rndTargetSpeed < rndSpeed ) {
            for ( var i = rndSpeed; i > rndTargetSpeed; i-- ) {
                returnTicks += this.deceleration[i];
            }
        }
        return returnTicks;
    },
    nextTurnSetPiece: function () {
        var index = this.me.race.currentPiece.index;
        var currentPiece = this.currentPiece();
        var inTurn = currentPiece.isCurved();
        for ( var i = index + 1; i < this.race.track.pieces.length; i++ ) {
            var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
            if ( possiblePiece.isStraight() && inTurn ) {
                inTurn = !inTurn;
            } else if ( possiblePiece.isCurved() && ( !inTurn || possiblePiece.angleSign() != currentPiece.angleSign() ) ) {
                return possiblePiece;
            }
        }
        for ( var i = 0; i < index; i++ ) {
            var possiblePiece = new Piece( i, this.race.track.pieces[i], this.me );
            if ( possiblePiece.isStraight() && inTurn ) {
                inTurn = !inTurn;
            } else if ( possiblePiece.isCurved() && ( !inTurn || possiblePiece.angleSign() != currentPiece.angleSign() ) ) {
                return possiblePiece;
            }
        }
    },

    availableLanes: function ( currentLaneIndex ) {
        //this will tell you if there is a lane to your left, a lane to the right, or both.
        var currentDistance = this.race.track.lanes[currentLaneIndex].distanceFromCenter;
        var i = 0;
        var canMoveLeft = false;
        var canMoveRight = false;
        while ( ( !canMoveLeft || !canMoveRight ) && i < this.race.track.lanes.length ) {
            var lane = this.race.track.lanes[i];
            if ( !canMoveLeft && lane.distanceFromCenter < currentDistance ) {
                canMoveLeft = true;
            } else if ( !canMoveRight && lane.distanceFromCenter > currentDistance ) {
                canMoveRight = true;
            }
            i++;
        }
        if ( !canMoveLeft && !canMoveRight ) {
            console.log( 'There is a problem with available lanes, or this is a one lane track.  Pretty boring race if it is.' );
        }
        return {
            'canMoveLeft': canMoveLeft,
            'canMoveRight': canMoveRight
        };
    },

    canMoveLeft: function ( currentLaneIndex ) {
        return this.availableLanes( currentLaneIndex ).canMoveLeft;
    },

    canMoveRight: function ( currentLaneIndex ) {
        return this.availableLanes( currentLaneIndex ).canMoveRight;
    },

    speed: function ( distanceFromCenter ) {
        var gameTickChange = this.me.stats.gameTick - this.me.stats.prev.gameTick;
        if ( this.me.race.prevPiece.index != this.me.race.currentPiece.index ) {
            var piece = this.race.track.pieces[this.me.race.prevPiece.index];
            var length = piece.length;
            if ( !length ) {
                var leftCurve = piece.angle < 0
                var additionalRadius = leftCurve ? distanceFromCenter : ( distanceFromCenter * -1 );
                var circumference = 3.14 * 2 * ( piece.radius + additionalRadius );
                var angle = piece.angle;
                if ( angle < 0 ) {
                    angle = angle * -1;
                }
                length = circumference / ( 360 / angle );
            }
            var distanceRemained = length - this.me.race.prevPiece.distance;
            var totalTraveled = distanceRemained + this.me.race.currentPiece.distance;
            return totalTraveled / gameTickChange;
        }

        return ( this.me.race.currentPiece.distance - this.me.race.prevPiece.distance ) / gameTickChange;
    },

    // Helper Methods
    shouldSlow: function () {

        if ( this.me.stats.speed <= 6.5 && this.me.stats.speed > -10 ) return false;

        var currentPiece = this.currentPiece();

        var nextPieces = [];
        var curr = null;
        var ind = 0;
        while ( curr == null || !curr.isCurved() ) {
            curr = this.nextPiece( ind );
            nextPieces.push( curr );
            ind++;
        }

        var distanceToCorner = 0;
        for ( var i = nextPieces.length - 1; i--; ) {
            distanceToCorner += nextPieces[i].distance;
        }

        var speedDecline = 0.145;
        var gameTicksLeft = Math.floor( distanceToCorner / this.me.stats.speed );

        // How far the car will travel at the current speed
        var distWillTravel = gameTicksLeft * this.me.stats.speed;

        // How far the car will travel if throttle is set to 0
        var canSlowDownDistance = gameTicksLeft * speedDecline;

        // The difference of change between not slowing down and slowing down
        var diff = distWillTravel - canSlowDownDistance;

        if ( diff < 10 )
            console.log( distWillTravel, canSlowDownDistance, diff );
        // Find distance until crash at current speed
        var angleOfCurve = nextPieces[nextPieces.length - 1].angle;
        if ( angleOfCurve >= 44 ) {
            if ( diff <= 1 ) {
                return true;
            }
        } else {
            if ( diff < 10 ) {
                return true;
            }
        }
        // Formula for speed to curve angle
        // if ( speed to angle > curve angle ) then return true;

        return false;
    },

    // Message Type Functions

    carPositions: function ( data, gameTick ) {
        // Lets not move until the race has started
        // Probably a disqualification check here
        if ( !this.raceStarted ) return;

        this.setThrottle( 0.6 );
    },

    gameInit: function ( data ) {
        this.race = data.race;
        console.log( 'gameInit', data.race.cars );
        this.me.stats.isSwitching = false;
        this.me.race.turbo.isEngaged = false;
    },

    joined: function ( data ) {
        console.log( 'joined', data );
    },

    yourCar: function ( data, gameId ) {
        console.log( 'Car Received: ' + data.name + ' (' + data.color + ')' );
        console.log( data );
        this.me.color = data.color;
        this.me.name = data.name;
        this.gameId = gameId;
    },
    turboAvailable: function ( data, gameTick ) {
        console.log( 'Turbo Available' );
        this.me.race.turbo.isAvailable = true;
        this.me.race.turbo.durationMillis = data.turboDurationMilliseconds;
        this.me.race.turbo.durationTicks = data.turboDurationTicks;
        this.me.race.turbo.factor = data.turboFactor;
        this.me.race.turbo.gameTick = gameTick;
    },
    crash: function ( data, gameTick ) {
        if ( data.color === this.me.color ) {
            console.log( 'Crash' );
            this.me.stats.crashes++;
            if (this.me.race.turbo.isEngaged === true) {
                this.me.stats.turboLimit += 50;
            } else {
                this.me.stats.throttleLimit += .5;
            }
            this.me.stats.isSwitching = false;
            this.me.race.turbo.isEngaged = false;
        }
    },
    turboStart: function ( data, gameTick ) {
        if ( data.color === this.me.color ) {
            console.log( 'Turbo Start' );
            this.me.race.turbo.isEngaged = true;
        }
    },
    turboEnd: function ( data, gameTick ) {
        if ( data.color === this.me.color ) {
            console.log( 'Turbo End' );
            this.me.race.turbo.isEngaged = false;
        }
    },  

    writeToExport: function ( data, lineDone ) {
        fs.appendFileSync( 'c:\\export\\export.txt', data );
        if ( lineDone ) {
            fs.appendFileSync( 'c:\\export\\export.txt', '\r\n' );
        } else {
            fs.appendFileSync( 'c:\\export\\export.txt', '\t' );
        }
    },

    deleteExport: function () {
        if ( fs.existsSync( 'c:\\export\\export.txt' ) )
            fs.unlinkSync( 'c:\\export\\export.txt' );
    },

    // Message Processor - Passes off to the correct function
    process: function ( msg ) {
        if ( !this.bot ) return;
        this.socket && this.socket.emit( 'data', msg )

        if ( msg.msgType === 'carPositions' ) {
            this.carPositions( msg.data, msg.gameTick );
        } else {
            if ( msg.msgType === 'gameInit' ) {
                this.gameInit( msg.data );
                //this.deleteExport();
            } else if ( msg.msgType === 'join' ) {
                this.socket && this.socket.emit( 'joined', msg );
                this.joined( msg.data );
            } else if ( msg.msgType === 'gameStart' ) {
                console.log( 'Game Started' );
                this.raceStarted = true;
                this.raceEnded = false;
            } else if ( msg.msgType === 'gameEnd' ) {
                console.log( 'Game Ended' );
                this.raceStarted = true;
                this.raceEnded = true;
            } else if ( msg.msgType === 'yourCar' ) {
                this.yourCar( msg.data, msg.gameId );
            } else if ( msg.msgType === 'turboAvailable' ) {
                this.turboAvailable( msg.data, msg.gameTick );
            } else if ( msg.msgType === 'crash' ) {
                this.crash( msg.data, msg.gameTick );
            } else if ( msg.msgType === 'turboStart' ) {
                this.turboStart( msg.data, msg.gameTick );
            } else if ( msg.msgType === 'turboEnd' ) {
                this.turboEnd( msg.data, msg.gameTick );
            } else {
                console.log( 'Unkown Message', msg, true );
            }

            this.bot.send( {
                msgType: "ping",
                data: {}
            });
        }
    }
}

module.exports = Handler;