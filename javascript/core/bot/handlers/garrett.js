// HANDLER SETUP FUNCTIONALITY
var handler = require('../handler'),
	extend = require('./extend');
var Handler = function(socket) {
	this.socket = socket;
};
Handler.prototype = extend(true, {}, handler.prototype);

// MODIFY CODE FROM HERE DOWN
// These will override the prototype functions from the default handler
Handler.prototype.carPositions = function( data, gameTick ) {
	// Lets not move until the race has started
	// Probably a disqualification check here
	if(!this.raceStarted) return;

	this.me.race.prevPiece.index = this.me.race.currentPiece.index;
	this.me.race.prevPiece.distance = this.me.race.currentPiece.distance;
	this.me.stats.prev.gameTick = this.me.stats.gameTick;
	this.me.stats.gameTick = gameTick;

	for(var i = 0; i < data.length; i++) {
		var car = data[i];

		if(car.id.color === this.me.color) {
			// This is my cars positioning data
			this.me.race.currentPiece.index = car.piecePosition.pieceIndex;
			this.me.race.currentPiece.distance = car.piecePosition.inPieceDistance;
		} else {
			// This is another cars position data
		}
	}

	var currentPiece = this.currentPiece();
	var nextPiece = this.nextPiece();
	var nextNextPiece = this.nextPiece(1);

	this.me.stats.speed = (this.me.race.currentPiece.distance - this.me.race.prevPiece.distance) / (this.me.stats.gameTick - this.me.stats.prev.gameTick);

	var slow = this.shouldSlow();
	if(slow == 2) {
	 	this.setThrottle( 6.5 );
	} else if(slow) {
	 	this.setThrottle( 0.0 );
	
 	} else {
	 	this.setThrottle( 1.0 );
 	}
	// if(nextPiece.isStraight()
	// 	&& nextNextPiece.isCurved()
	// 	&& this.me.stats.speed > 9) {
	// 		this.setThrottle( 0.0 );
	// } else if(currentPiece.isStraight()
	// 	&& nextPiece.isCurved()
	// 	&& (nextPiece.angle > 25 || nextPiece.angle < -25)) {

	// 	if(this.me.stats.speed <= 8) {
	// 		this.setThrottle( 0.8 );
	// 	} else {
	// 		this.setThrottle( 0.0 );
	// 	}

	// } else if(currentPiece.isStraight() && nextPiece.isStraight()) {
	// 	this.setThrottle( 1.0 );
	// }
	// else {
	// 	this.setThrottle( 0.8 );
	// }
};


// LEAVE THIS HERE
module.exports = Handler;