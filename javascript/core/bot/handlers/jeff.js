// HANDLER SETUP FUNCTIONALITY
var handler = require('../handler'),
    RaceCarDriver = require('../raceCarDriver');
	extend = require('./extend');
var Handler = function(socket) {
	this.socket = socket;
};
Handler.prototype = extend(true, {}, handler.prototype);

// MODIFY CODE FROM HERE DOWN
// These will override the prototype functions from the default handler
Handler.prototype.carPositions = function( data, gameTick ) {
	// Lets not move until the race has started
	// Probably a disqualification check here
    if (!this.raceStarted) return;

    var car = data[0];
    var laneRadius = 0;
    var distanceFromCenter = 0;
    var jerry = new RaceCarDriver(this.race.track);

    for (var i = 0; i < data.length; i++) {
        if (data[i].id.color === this.me.color) {
            // This is my cars positioning data
            car = data[i];
            jerry.myCar = car;
            var prevPiece = this.currentPiece();
            var prevLength = prevPiece.length;
            if (prevPiece.isCurved()) {
                prevLength = prevPiece.angle * (Math.PI / 180) * (prevPiece.radius + this.race.track.lanes[this.me.stats.prev.lane].distanceFromCenter);
                laneRadius = prevPiece.radius + this.race.track.lanes[this.me.stats.prev.lane].distanceFromCenter;
            }
            this.me.stats.prev.length = prevLength;

            if (this.me.race.currentPiece.index != car.piecePosition.pieceIndex) {
                //this.me.stats.distanceDelta = Math.abs((this.me.stats.prev.length - this.me.stats.prev.inPieceDistance) + car.piecePosition.inPieceDistance);

            } else {
                this.me.stats.distanceDelta = (car.piecePosition.inPieceDistance - this.me.stats.prev.inPieceDistance);
            }
            this.me.stats.totalDistance += this.me.stats.distanceDelta;
            this.me.race.currentPiece.index = car.piecePosition.pieceIndex;
            this.me.race.currentPiece.distance = car.piecePosition.inPieceDistance;
        } else {
            jerry.addOtherCar(data[i]);
        }
    }

    var carAngleDelta = Math.abs(car.angle) - Math.abs(this.me.stats.prev.anlge);
    if (car.angle > 0 && this.me.stats.prev.anlge < 0) {
        carAngleDelta = car.angle + Math.abs(this.me.stats.prev.anlge);
    } else if (car.angle < 0 && this.me.stats.prev.anlge > 0) {
        carAngleDelta = Math.abs(car.angle) + this.me.stats.prev.anlge;
    }

    this.me.stats.prev.anlge = car.angle;
    distanceFromCenter = this.race.track.lanes[car.piecePosition.lane.startLaneIndex].distanceFromCenter;
    var currentPiece = this.currentPiece();
    var nextPiece = this.nextPiece(1);
    var nextNextPiece = this.nextPiece(2);
    var nextNextNextPiece = this.nextPiece(3);
    var nextTurnPiece = this.nextTurnSetPiece();
    var nextTurn = this.nextTurnPiece();
    this.me.stats.prev.gameTick = this.me.stats.gameTick
    this.me.stats.gameTick = gameTick;
    this.me.stats.prev.inPieceDistance = car.piecePosition.inPieceDistance;
    this.me.stats.inPieceDistance = car.piecePosition.inPieceDistance;
    if (this.me.stats.prev.lane != car.piecePosition.lane.startLaneIndex) this.me.stats.isSwitching = false;
    this.me.stats.prev.lane = car.piecePosition.lane.startLaneIndex;
    this.me.stats.speed = this.me.stats.distanceDelta / (this.me.stats.gameTick - this.me.stats.prev.gameTick);

    var ticksToNextTurn = this.ticksToNextTurnPiece();
    var availableLanes = this.availableLanes(car.piecePosition.lane.startLaneIndex);
    jerry.availableLanes = availableLanes;
    jerry.isAlreadySwitching = this.me.stats.isSwitching;
    jerry.nextTurnPiece = nextTurnPiece;

    var shouldSwitchLanes = jerry.shouldSwitchLanes();
    if (shouldSwitchLanes.change) {
        this.switchLane(shouldSwitchLanes.direction);
    } else {
        if (Math.abs(car.angle) > 40) {
            this.setThrottle(0.0);
        } else if (currentPiece.isCurved()) {
            if (Math.abs(carAngleDelta) > 3) {
                this.setThrottle(0.0);
            } else if (carAngleDelta > 1.5 && Math.abs(car.angle) > 30) {
                this.setThrottle(0.0);
            } else if (nextPiece.isCurved()
                && (Math.abs(nextPiece.angle) > Math.abs(currentPiece.angle) || Math.abs(currentPiece.angle - nextPiece.angle) > Math.abs(currentPiece.angle))
                && this.me.stats.speed > (nextPiece.curveLength(distanceFromCenter) / 12)) {
                this.setThrottle(0.3);
            } else if (nextPiece.isCurved() && this.me.stats.speed > (nextPiece.curveLength(distanceFromCenter) / 12)) {
                this.setThrottle(0.5);
            } else {
                this.setThrottle(1.0);
            }
        } else if (currentPiece.isStraight()
            && ticksToNextTurn <= this.ticksToDecelrateTo((nextTurn.curveLength(distanceFromCenter) / 10.6))
            && this.me.stats.speed > (nextTurn.curveLength(distanceFromCenter) / 10.6)) {
            this.setThrottle(0.0);
        } else if (currentPiece.isStraight()
            && ticksToNextTurn <= this.ticksToDecelrateTo((nextTurn.curveLength(distanceFromCenter) / 12))
            && Math.abs(car.angle) > 0
            && this.me.stats.speed > (nextTurn.curveLength(distanceFromCenter) / 12)) {
            this.setThrottle(0.0);
        } else {
            this.setThrottle(1.0);
        }
    }

};


// LEAVE THIS HERE
module.exports = Handler;