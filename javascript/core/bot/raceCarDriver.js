/**
 * Created by jtimr_000 on 4/27/2014.
 * This gives a place to make decisions based on the other racers - for example, if there is another car in your
 * lane and you want to try to pass, or use that car to slow your car, or maybe you noticed that you were slipping
 * a lot around that last bend and you want to lower your throttle limit while in that curve.
 */
var RaceCarDriver = function(track) {
    this.track = track;
}
RaceCarDriver.prototype = {
    carPositions: {},
    myCar: {},
    otherCars: [],
    carsInMyLane: [],
    track: {},
    availableLanes: {},
    isAlreadySwitching: false,
    nextTurnPiece: {},
    isCarMovingIntoMyLane: false,
    isCarMovingOutOfMyLane: false,
    isCarInFrontOfMe: false,
    carMovingInStart: 0,
    isCarInRightLane: false,
    isCarInLeftLane: false,

    addOtherCar: function(car) {
        this.otherCars.push(car);
    },
    findLane: function(currentLaneDistance, currentLaneIndex, laneIndexToFind) {
        for (var i = 0; i < this.track.lanes.length; i++) {
            if (this.track.lanes[i].index == laneIndexToFind) {
                if (currentLaneIndex < laneIndexToFind
                    && this.track.lanes[i].distanceFromCenter > currentLaneDistance) {
                    return this.track.lanes[i];
                } else if (currentLaneIndex > laneIndexToFind
                    && this.track.lanes[i].distanceFromCenter < currentLaneDistance) {
                    return this.track.lanes[i];
                }
            }
        }
    },
    shouldSwitchLanes: function() {
        var laneChange = {change: false};
        var myCarPosition = this.myCar.piecePosition;
        var myTrackPiece = this.track.pieces[myCarPosition.pieceIndex];
        var nextIndex = (myCarPosition.pieceIndex + 1) < this.track.pieces.length ? myCarPosition.pieceIndex : 0;
        var myNextTrackPiece = this.track.pieces[nextIndex];
        if (myNextTrackPiece.switch && !this.isAlreadySwitching) {
            for (var i = 0; i < this.otherCars.length; i++) {
                var otherCarPosition = this.otherCars[i].piecePosition;

                if (otherCarPosition.pieceIndex === nextIndex
                    && otherCarPosition.lane.endLaneIndex === myCarPosition.startLaneIndex) {
                    this.isCarMovingIntoMyLane = true;
                    this.carMovingInStart = otherCarPosition.lane.startLaneIndex;
                }
                if (otherCarPosition.pieceIndex === nextIndex
                    && (otherCarPosition.lane.endLaneIndex === myCarPosition.startLaneIndex + 1
                        || otherCarPosition.lane.endLaneIndex === myCarPosition.startLaneIndex - 1)) {
                    this.isCarMovingOutOfMyLane = true;
                }
                if (otherCarPosition.pieceIndex === myCarPosition.pieceIndex
                    && myCarPosition.inPieceDistance < otherCarPosition.inPieceDistance) {
                    this.isCarInFrontOfMe = true;
                }
                if ((otherCarPosition.pieceIndex === nextIndex
                    || (otherCarPosition.pieceIndex === myCarPosition.pieceIndex
                        && myCarPosition.inPieceDistance < otherCarPosition.inPieceDistance))
                    && (otherCarPosition.lane.endLaneIndex === myCarPosition.startLaneIndex + 1)) {
                    this.isCarInRightLane = true;
                }
                if ((otherCarPosition.pieceIndex === nextIndex
                    || (otherCarPosition.pieceIndex === myCarPosition.pieceIndex
                        && myCarPosition.inPieceDistance < otherCarPosition.inPieceDistance))
                    && (otherCarPosition.lane.endLaneIndex === myCarPosition.startLaneIndex - 1)) {
                    this.isCarInLeftLane = true;
                }
            }
            if (this.isCarMovingIntoMyLane || this.isCarInFrontOfMe) {
                if (this.isCarMovingIntoMyLane) {
                    if (this.carMovingInStart < this.myCar.piecePosition.startLaneIndex
                        && this.availableLanes.canMoveRight && this.nextTurnPiece.angle > 0) {
                        laneChange =  {change: true, direction: 'Right'};
                    } else if (this.carMovingInStart > this.myCar.piecePosition.startLaneIndex
                        && this.availableLanes.canMoveLeft && this.nextTurnPiece.angle < 0) {
                        laneChange = {change: true, direction: 'Left'};
                    }
                }

                if (this.isCarInFrontOfMe) {
                    if (this.availableLanes.canMoveLeft
                        && this.nextTurnPiece.angle < 0
                        && !this.isCarInLeftLane) {
                        laneChange = {change: true, direction: 'Left'};
                    } else if (this.availableLanes.canMoveRight
                        && this.nextTurnPiece.angle > 0
                        && !this.isCarInRightLane) {
                        laneChange =  {change: true, direction: 'Right'};
                    }
                }

            } else if (this.nextTurnPiece.angle < 0 && this.availableLanes.canMoveLeft
                && !this.isCarInLeftLane) {
                laneChange = {change:true, direction: 'Left'};
            } else if (this.nextTurnPiece.angle > 0 && this.availableLanes.canMoveRight
                && !this.isCarInRightLane) {
                laneChange = {change:true, direction: 'Right'};
            }
        }

        return laneChange;
    }


}

module.exports = RaceCarDriver;
