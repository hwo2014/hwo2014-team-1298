function Piece(index, piece, me) {
    this.piece = piece;
    this.length = (piece.length === undefined) ? 0 : piece.length;
    this.angle = (piece.angle === undefined) ? 0 : piece.angle;
    this.radius = (piece.radius === undefined) ? 0 : piece.radius;
    this.switchFlag = (piece.switch === undefined) ? false : piece.switch;
    this.index = index;

    if (index === me.race.currentPiece.index) {
        // this is the current piece
        // Gets distance into piece in a [0...1] range
        this.distance = me.race.currentPiece.distance / piece.length;
    }
}

Piece.prototype = {
    piece: null,
    distance: 0,
    angle: 0,
    switchFlag: false,
    length: 0,
    index: 0,


    isStraight: function () {
        return this.piece.angle === undefined && this.piece.radius === undefined;
    },
    isCurved: function () {
        return !this.isStraight();
    },
    curveLength: function (offset) {
        if (!offset) offset = 0;
        return Math.abs(this.angle) * (Math.PI / 180) * (this.radius + offset);
    },
    isLeftCurve: function () {
        return this.isCurved() && this.angle < 0;
    },
    isRightCurve: function () {
        return !this.isLeftCurve();
    },
    angleSign: function () {
        return this.angle / Math.abs(this.angle);
    }
};

module.exports = Piece;