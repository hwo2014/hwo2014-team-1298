// Application Entry Point
// Either sets up the web server
// Or runs the bot straight up

// All code for the AI should changed within /core/
// core/bot/handler.js is where most of the AI starts

var net = require("net"),
  JSONStream = require('JSONStream'),
  express = require( 'express' ),
  config = require( './config/config' ),
  bot = require( './core/bot/bot' ),
  garrett = require( './core/bot/handlers/garrett' ),
  jeff = require( './core/bot/handlers/jeff' ),
  karl = require( './core/bot/handlers/karl' );

// In order for this to be started NODE_ENV=web must be set
// via the terminal 'NODE_ENV=web node index.js'
if(config.web) {
    var app = express();
    require('./config/express')(app);
    require('./config/routes')(app);
    require('./config/sockets')(app);
} else {
    console.log("I'm", config.botName, "and connect to", config.serverHost + ":" + config.serverPort);
    var handler = null;
    // uncomment one of the lines below for the CI to use it as the handler of choice
    //handler = new garrett();
    // handler = new jeff();
    handler = new karl();
    var bot = new bot( handler );
    bot.connect();
}