var app = angular.module('HOW', []);

angular.module('HOW').factory('socket', ['$rootScope', function ($rootScope) {
  var socket = io.connect();
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      });
    }
  };
}]);

angular.module('HOW').controller('MainCtrl', ['$scope', 'socket', function($scope, socket) {
	$scope.test = 'Tada';

	$scope.data = [];
	$scope.colors = [];
	$scope.cars = {};
	$scope.pieces = [];
	$scope.laps = [];
	$scope.crashes = [];
	$scope.botHandler = 'default';

	$scope.start = function() {
		console.log('Starting');
		socket.emit('connect', $scope.botHandler, function () {
			socket.emit('start');
		});
	}

	socket.on('data', function(msg) {
		if(msg.msgType == 'carPositions') {
			var data = msg.data;
			console.log(data);
			for(var i = data.length; i--;) {
				if(!$scope.cars[data[i].id.color]) {
					$scope.colors.push(data[i].id.color);
					$scope.cars[data[i].id.color] = {
						pieceIndex : data[i].piecePosition.pieceIndex,
						inPieceDistance: data[i].piecePosition.inPieceDistance,
						gameTick: msg.gameTick,
						speed: 0,
						angle: 0
					};
				} else {
					var car = $scope.cars[data[i].id.color];
					car.pieceIndex = data[i].piecePosition.pieceIndex;
					car.prev_inPieceDistance = car.inPieceDistance;
					car.inPieceDistance = data[i].piecePosition.inPieceDistance;
					car.prev_gameTick = car.gameTick;
					car.gameTick = msg.gameTick;
					car.speed = (car.inPieceDistance - car.prev_inPieceDistance) / (car.gameTick - car.prev_gameTick);
					car.lane = data[i].piecePosition.lane.startLaneIndex;
					car.angle = data[i].angle;
				}
			}
		} else if(msg.msgType == 'gameInit') {
			console.log(msg);
			$scope.pieces = msg.data.race.track.pieces;			
		} else if(msg.msgType == 'lapFinished') {
			console.log(msg);
			$scope.laps.push(msg.data);		
		} else if(msg.msgType == 'crash') {
			console.log('Crash', msg);
			var car = $scope.cars[msg.data.color];
			msg.data.me = {
				angle: car.angle,
				speed: car.speed,
				color: msg.data.color,
				piece: car.pieceIndex
			};
			msg.data.gameTick = msg.gameTick;
			$scope.crashes.push(msg.data);		
		} else {
			console.log(msg);
			$scope.data.unshift(msg);
		}
	})

	socket.on('connect', function() {
	    console.log( 'Socket was connected.' );

	});

	window.scope = $scope;
}]);